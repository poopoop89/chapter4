package com.rubypaper.Chapter4;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 * Entity implementation class for Entity: Board
 *
 */
public class JPAClient {
	public static void main (String[] args) {
		EntityManagerFactory emf= Persistence.createEntityManagerFactory("Chapter4");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		try {
			tx.begin();
			Board board = new Board();
			board.setTitle("JPA제목");
			board.setWriter("관리자");
			board.setContent("JPA 글등록 잘되요");
			board.setSeq(0L);
			board.setCreateDate(new Date());
			
			em.merge(board);
			tx.commit();
		}catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}finally {
			em.close();
			emf.close();
		}
		
	}
	   
}
